# SRE_STEELEYE

Tools and Services Used:

1. AWS services (VPC, EC2 , Launch configuration)
2. Infra as a Code(IAC) tools - Terraform 
3. Container Tool- Docker
4. Version Control - Git
5. Scripting -Shell
6. End to End Automation - Gitlab CI/CD
7. Nginx Loadbalancer - ngix LB 

Instructions
Kindly provide AWS credentials in the main.tf to setup infrastructure and run the applications on the 2 application servers 
